import {Application, Container, Graphics, Sprite, Text} from 'pixi.js';
import FontFaceObserver from 'fontfaceobserver';
import gsap, {Back, Expo} from 'gsap';

import {Colors} from '~/dictionary';

class LoadingScreen {
  progress: number;
  app: Application;
  screen: Container;
  timeLine: GSAPTimeline;
  shutter: Graphics;
  text?: Text;
  progressText?: Text;
  loadingFigure: Sprite;

  constructor() {
    const screenWidth = window.innerWidth || document.body.clientWidth;
    const screenHeight = window.innerHeight || document.body.clientHeight;
    const midX = Math.round(screenWidth / 2);
    const midY = Math.round(screenHeight / 2);

    this.progress = 0;

    this.app = new Application({
      width: screenWidth,
      height: screenHeight,
      backgroundColor: Colors.Black,
    });
    document.body.appendChild(this.app.view);

    /**
     * Create main scene
     */
    this.screen = new Container();
    this.app.stage.addChild(this.screen);
    this.timeLine = gsap.timeline();
    /**
     * Absolutely dark screen to overflow other group
     */
    this.shutter = new Graphics();

    /**
     * Add it to scene asap
     */
    this.shutter.beginFill(Colors.Black, 1);
    this.shutter.drawRect(0, 0, screenWidth, screenHeight);
    this.shutter.endFill();

    const background = new Graphics();
    background.beginFill(Colors.Black, 1);
    background.drawRect(0, 0, screenWidth, screenHeight);
    background.endFill();

    const fontLoadCallback = () => {
      this.text = new Text('LOADING', {
        fontFamily: 'Knewave',
        fontSize: '50px',
        fill: Colors.White,
      });
      this.text.position.x = 0;
      this.text.position.y = midY / 2;
      this.screen.addChild(this.text);

      this.progressText = new Text('0%', {
        fontFamily: 'Knewave',
        fontSize: '24px',
        fill: Colors.White,
      });
      this.progressText.anchor.set(0.5, 0.5);
      this.progressText.position.x = midX;
      this.progressText.position.y = screenHeight - 150;
      this.screen.addChild(this.progressText);

      gsap.to(this.shutter, {duration: 1, alpha: 0});
      /**
       * Move text from left side to center
       */
      this.timeLine.to(this.text, {
        duration: 0.5,
        alpha: 1,
        x: midX - this.text.width / 2,
        ease: Back.easeOut.config(2),
      });
      /**
       * Appear the circle part
       */
      this.timeLine.to(this.loadingFigure, {
        duration: 2,
        alpha: 1,
        height: 80,
        width: 80,
        ease: Expo.easeOut,
      });
    };

    const font = new FontFaceObserver('Knewave');
    const font2 = new FontFaceObserver('AppleTea');
    Promise.all([font.load(), font2.load()]).then(fontLoadCallback);

    // Create rotating figure
    this.loadingFigure = Sprite.from('images/loading2.png');
    this.loadingFigure.alpha = 0;
    this.loadingFigure.width = 10;
    this.loadingFigure.height = 10;
    this.loadingFigure.position.x = midX - this.loadingFigure.width / 2;
    this.loadingFigure.position.y = midY;
    /**
     * Set anchor to rotate sprite by center point
     */
    this.loadingFigure.anchor.x = 0.5;
    this.loadingFigure.anchor.y = 0.5;
    /**
     * Add elements to scene
     */
    this.app.stage.addChild(this.shutter);
    this.screen.addChild(background);
    this.screen.addChild(this.loadingFigure);
  }

  update = () => {
    if (!this.app.stage) {
      return false;
    }
    this.loadingFigure.rotation += 0.2;
    if (this.progressText) {
      if (this.progress === 100 && this.progressText.visible) {
        this.progressText.visible = false;
      } else {
        this.progressText.text = `${this.progress.toFixed()}%`;
      }
    }
    this.app.render();
    requestAnimationFrame(this.update);
  };

  show() {
    requestAnimationFrame(this.update);
  }

  hide = () =>
    new Promise((resolve) => {
      this.timeLine.to(this.loadingFigure, {
        duration: 1,
        alpha: 0,
      });
      if (this.text) {
        this.timeLine.to(this.text, {
          duration: 0.5,
          x: 2000,
          ease: Back.easeIn.config(2),
        });
      }
      this.timeLine.to(this.shutter, {
        duration: 0.5,
        alpha: 1,
        onComplete: () => {
          resolve(1);
          document.body.removeChild(this.app.view);
          this.app.destroy();
        },
      });
    });
}

export default LoadingScreen;
