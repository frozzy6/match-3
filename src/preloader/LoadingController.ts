import {AssetsResult} from '~/types/assets';

import LoadingScreen from './LoadingScreen';
import Preloader from './AssetsPreloader';

class LoadingController {
  screen: LoadingScreen;
  preloader: Preloader;

  constructor() {
    this.preloader = new Preloader();
    this.screen = new LoadingScreen();
    this.preloader.loader.onProgress.add(({progress}: any) => {
      this.screen.progress = parseInt(progress.toFixed(), 10);
    });

    this.screen.show();
  }

  load = async (onLoad: (assts: AssetsResult) => void) => {
    const assets = await this.preloader.loadResources();

    await this.screen.hide();

    onLoad(assets);
  };
}

export default LoadingController;
