import {TAssetItem, Asset} from '~/types/assets';

export const AssetsMapped: TAssetItem[] = [
  {
    name: Asset.START_MENU_BACKGROUND,
    path: 'images/menu/background.png',
  },
  {
    name: Asset.START_MENU_GROUND,
    path: 'images/menu/ground.png',
  },
  {
    name: Asset.START_MENU_TREE,
    path: 'images/menu/distant_trees_3.png',
  },
  {
    name: Asset.START_MENU_TREES,
    path: 'images/menu/trees and bushes_2.png',
  },
  {
    name: Asset.CLOUD_1,
    path: 'images/menu/clouds/cloud_1.png',
  },
  {
    name: Asset.CLOUD_2,
    path: 'images/menu/clouds/cloud_2.png',
  },
  {
    name: Asset.CLOUD_3,
    path: 'images/menu/clouds/cloud_3.png',
  },
  {
    name: Asset.CLOUD_4,
    path: 'images/menu/clouds/cloud_4.png',
  },
  {
    name: Asset.CLOUD_5,
    path: 'images/menu/clouds/cloud_5.png',
  },
  {
    name: Asset.MENU_BUTTON,
    path: 'images/button.png',
  },
  {
    name: Asset.MENU_BUTTON_PRESSED,
    path: 'images/button-pressed.png',
  },
  {
    name: Asset.WIND_PARTICLE,
    path: 'images/wind.png',
  },
  {
    name: Asset.LANDSCAPE,
    path: 'images/landscape.png',
  },
  {
    name: Asset.LEVEL_1,
    path: 'levels/level1.json',
  },
  {
    name: Asset.LEVEL_2,
    path: 'levels/level2.json',
  },
  {
    name: Asset.LEVEL_3,
    path: 'levels/level3.json',
  },
];
