import {Loader} from 'pixi.js';

import {AssetsResult, TAssetItem} from '~/types/assets';

import {AssetsMapped} from './assets';

/**
 * Its a proxy between native pixi loader and this application
 */
class Preloader {
  model: TAssetItem[];
  loader: Loader;

  constructor() {
    this.model = AssetsMapped;
    this.loader = new Loader();
  }

  loadResources = () =>
    new Promise<AssetsResult>((resolve) => {
      // this.loader.onComplete.add((loader) => resolve(loader.resources));

      /**
       * Parse model and add each object to pixi loader
       */
      for (let i = 0; i < this.model.length; ++i) {
        const {name, path} = this.model[i];
        this.loader.add(name, path);
      }

      this.loader.load((_, resources) => resolve(resources as AssetsResult));
    });
}

export default Preloader;
