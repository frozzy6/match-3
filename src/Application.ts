import {gsap} from 'gsap';
import {MotionPathPlugin} from 'gsap/all';

import event, {Events} from '~/utils/event';
import {AssetsResult} from '~/types/assets';
import {Scene} from '~/types/scenes';

import {TOptions} from './types';
import LoadingController from './preloader/LoadingController';
import SceneManager from './main/SceneManager';

class Application {
  options: TOptions;

  constructor(options: TOptions) {
    this.options = options;

    gsap.registerPlugin(MotionPathPlugin);
    // Create loader instance for preload assets and show shutter
    const loadingController = new LoadingController();

    // Start preload resources and show the loading screen
    loadingController.load((assets) => this.start(assets));
  }

  start(assets: AssetsResult) {
    const sceneManager = new SceneManager(assets);

    event.on(Events.StartNewGame, () => sceneManager.showScene(Scene.Game));
    event.on(Events.Continue, () => sceneManager.showScene(Scene.Game));
    event.on(Events.BackToMenu, () => sceneManager.showScene(Scene.Menu));
    event.on(Events.BackToMenu, () => sceneManager.showScene(Scene.Menu));
    event.on(Events.LevelsSceneShow, () => sceneManager.showScene(Scene.Levels));

    // Show the first scene
    sceneManager.showScene(Scene.Menu);
  }
}

export default Application;
