export enum Color {
  Red = 'RED',
  Green = 'GREEN',
  Blue = 'BLUE',
}

export interface TOptions {
  Grid: {
    width: number;
    height: number;
  };
  ItemsPool: Array<{color: Color}>;
}
