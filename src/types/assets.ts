import {LoaderResource} from 'pixi.js';

export enum Asset {
  START_MENU_BACKGROUND = 'START_MENU_BACKGROUND',
  START_MENU_GROUND = 'START_MENU_GROUND',
  START_MENU_TREE = 'START_MENU_TREE',
  START_MENU_TREES = 'START_MENU_TREES',
  CLOUD_1 = 'CLOUD_1',
  CLOUD_2 = 'CLOUD_2',
  CLOUD_3 = 'CLOUD_3',
  CLOUD_4 = 'CLOUD_4',
  CLOUD_5 = 'CLOUD_5',
  MENU_BUTTON = 'MENU_BUTTON',
  MENU_BUTTON_PRESSED = 'MENU_BUTTON_PRESSED',
  WIND_PARTICLE = 'WIND_PARTICLE',
  LANDSCAPE = 'LANDSCAPE',
  LEVEL_1 = 'LEVEL_1',
  LEVEL_2 = 'LEVEL_2',
  LEVEL_3 = 'LEVEL_3',
}

export interface TAssetItem {
  name: Asset;
  path: string;
}

export type AssetsResult = Record<Asset, LoaderResource>;

export interface GameLevel {
  name: string;
  jewels: number;
  quest: {
    turns: number;
    blocks: Array<{
      type: string;
      count: number;
    }>;
  };
  grid: string[][];
}
