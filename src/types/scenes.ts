import {Container, Graphics} from 'pixi.js';

export enum Scene {
  Menu = 'Menu',
  Game = 'Game',
  Levels = 'Levels',
}

export type ScenesMap = Record<Scene, AbscractScene>;

export interface VisibleParams {
  duration?: number;
  params?: Record<string, any>;
}

export abstract class AbscractScene {
  abstract container: Container;
  abstract debugContainer: Container;
  abstract shutter: Graphics;

  abstract show(arg?: VisibleParams): Promise<void>;
  abstract hide(arg?: VisibleParams): Promise<void>;
  update?(): void;
  resize?(screenWidth: number, screenHeight: number): void;
}
