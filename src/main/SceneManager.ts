import {Application} from 'pixi.js';

import {Colors} from '~/dictionary';
import {AbscractScene, Scene, ScenesMap} from '~/types/scenes';

import MenuScene from './sceneManager/MenuScene';
import GameScene from './sceneManager/GameScene';
import LevelsScene from './sceneManager/LevelsScene';

const SCENES_MAP: any = {
  [Scene.Menu]: MenuScene,
  [Scene.Game]: GameScene,
  [Scene.Levels]: LevelsScene,
};

class SceneManager {
  app: Application;
  scenes: ScenesMap;
  currenScene: AbscractScene | null;

  constructor(assets: any) {
    // Create container for holding all scenes
    this.scenes = {} as ScenesMap;
    this.currenScene = null;

    const screenWidth = window.innerWidth || document.body.clientWidth;
    const screenHeight = window.innerHeight || document.body.clientHeight;

    // Create main canvas
    this.app = new Application({
      width: screenWidth,
      height: screenHeight,
      backgroundColor: Colors.Black,
      resizeTo: window,
    });

    document.body.appendChild(this.app.view);

    // Create all scenes
    const mapedScenes = Object.entries(SCENES_MAP) as Array<[Scene, AbscractScene]>;

    mapedScenes.forEach(([key, SceneConstructor]) => {
      this.scenes[key] = new (SceneConstructor as any)(assets, screenWidth, screenHeight);
    });

    window.addEventListener('resize', this.resize);
    window.onorientationchange = this.resize;
    requestAnimationFrame(this.update);
  }

  update = () => {
    this.app.render();
    if (this.currenScene && this.currenScene.update) {
      try {
        this.currenScene.update();
      } catch (e) {
        console.log('Error during scene update');
        console.log(e);
      }
    }
    requestAnimationFrame(this.update);
  };

  async showScene(key: Scene, params?: Record<string, any>) {
    const {stage} = this.app;

    const sceneToShow = this.scenes[key];

    // If its a first scene in the game
    if (!this.currenScene) {
      stage.addChild(sceneToShow.container);
      sceneToShow.show({params});
    } else {
      await this.currenScene.hide().then(() => {
        if (this.currenScene) {
          stage.removeChild(this.currenScene.container);
        }
        stage.addChild(sceneToShow.container);
        sceneToShow.show({params});
      });
    }
    this.currenScene = sceneToShow;
  }

  resize = () => {
    window.scrollTo(0, 0);
    const width = window.innerWidth || document.body.clientWidth;
    const height = window.innerHeight || document.body.clientHeight;

    const {view} = this.app;
    view.style.height = `${height}px`;
    view.style.width = `${width}px`;

    Object.keys(this.scenes).forEach((key) => {
      const scene = this.scenes[key as Scene];

      if (scene.resize) {
        scene.resize(width, height);
      }
    });
  };
}

export default SceneManager;
