import {Container, Graphics, Text} from 'pixi.js';

import {Colors} from '~/dictionary';

export enum AjustChildren {
  Left = 1,
  Center,
  Right,
}

interface InfoProps {
  screen: Container;
  width: number;
  height: number;
  label: string;
  children?: Container;
  x?: number;
  y?: number;
  fontSize?: number;
  fill?: Colors | string;
  gap?: number;
  lineWidth?: number;
  ajust?: AjustChildren;
}

const PADDING = 2;

class Info {
  screen: Container;
  container: Container;
  text: Text;
  width: number;
  height: number;
  children?: Container;
  ajust?: AjustChildren;

  constructor({
    screen,
    label,
    fontSize,
    fill,
    width,
    height,
    x,
    y,
    children,
    gap,
    lineWidth,
    ajust,
  }: InfoProps) {
    this.screen = screen;
    this.container = new Container();
    this.container.position.set(x || 0, y || 0);
    this.width = width;
    this.height = height;
    this.children = children;
    this.ajust = ajust || AjustChildren.Center;

    const border = new Graphics();

    border.clear();
    border.lineStyle(lineWidth || 3, Colors.White);
    border.drawRoundedRect(0, 0, width, height, 10);

    const text = new Text(label, {
      fontFamily: 'AppleTea',
      fontSize: fontSize || 14,
      fill: fill || 'white',
    });
    text.anchor.set(0.5, 0.5);
    text.position.set(width / 2, 0);
    this.text = text;

    const padding = gap || PADDING;
    const mask = new Graphics();
    mask.beginFill(Colors.Red);
    mask.moveTo(-padding, -padding);
    mask.lineTo(width / 2 - text.width / 2 - padding, -padding);
    mask.lineTo(width / 2 - text.width / 2 - padding, text.height / 2);
    mask.lineTo(width / 2 + text.width / 2 + padding, text.height / 2);
    mask.lineTo(width / 2 + text.width / 2 + padding, -padding);
    mask.lineTo(width + padding, -padding);
    mask.lineTo(width + padding, height + padding);
    mask.lineTo(-padding, height + padding);
    mask.endFill();

    this.container.addChild(mask);
    border.mask = mask;

    this.container.addChild(border);
    this.container.addChild(text);
    this.screen.addChild(this.container);

    if (children) {
      this.container.addChild(children);
    }
    this.adjustChildren();
  }

  destroy() {
    this.screen.removeChild(this.container);
  }

  adjustChildren() {
    if (!this.children) {
      return;
    }
    const y = this.height / 2 - this.children.height / 2;

    if (this.ajust === AjustChildren.Left) {
      this.children.position.set(0, y);
    }
    if (this.ajust === AjustChildren.Center) {
      this.children.position.set(this.width / 2 - this.children.width / 2, y);
    }
    if (this.ajust === AjustChildren.Right) {
      this.children.position.set(this.width, y);
    }
  }
}

export default Info;
