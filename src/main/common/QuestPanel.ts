import {Container, Text} from 'pixi.js';

import {GameLevel} from '~/types/assets';

import Jewel from '../sceneManager/GameScene/Jewel';
import Info, {AjustChildren} from './Info';

interface Block {
  count: number;
  container: Container;
  jewel: Jewel;
  label: Text;
}

const TEXT_OPTIONS = {
  fontSize: '22px',
  fontFamily: 'AppleTea',
  fill: 'white',
};

class QuestPanel {
  container: Container;
  questInfo?: Info;
  turnsInfo?: Info;
  turns: number;
  turnsLabel: Text;
  blocksContainer: Container;
  blocks: Record<string, Block>;

  constructor(screen: Container, x: number, y: number) {
    this.container = new Container();
    this.container.position.set(x, y);
    this.blocks = {};
    this.blocksContainer = new Container();

    this.turns = 0;

    this.turnsLabel = new Text(this.turns.toString(), TEXT_OPTIONS);
    this.turnsLabel.position.set(20, 5);

    screen.addChild(this.container);
  }

  setQuestForLevel(quest: GameLevel['quest']) {
    const {turns, blocks} = quest;
    this.turns = turns;
    this.blocks = {};

    this.questInfo?.destroy();
    this.turnsInfo?.destroy();

    this.turnsInfo = new Info({
      screen: this.container,
      width: 80,
      height: 50,
      label: 'turns',
      children: this.turnsLabel,
    });

    this.questInfo = new Info({
      screen: this.container,
      x: 90,
      width: blocks.length * 90 + 10,
      height: 50,
      label: 'quest',
      children: this.blocksContainer,
      ajust: AjustChildren.Left,
    });

    this.blocksContainer.removeChildren();
    this.turnsLabel.text = turns.toString();
    blocks.forEach((item, index) => {
      const blockContainer = new Container();
      const j = new Jewel({
        id: item.type,
        x: 10 + index * 90,
        y: 15,
        width: 30,
        height: 30,
      });
      j.sprite.anchor.set(0, 0.5);

      const text = new Text(`x ${item.count}`, TEXT_OPTIONS);
      text.anchor.set(0, 0.5);
      text.position.set(45 + index * 90, 15);

      blockContainer.addChild(j.sprite);
      blockContainer.addChild(text);

      this.blocksContainer.addChild(blockContainer);
      const block: Block = {
        count: item.count,
        container: blockContainer,
        jewel: j,
        label: text,
      };
      this.blocks = {
        [item.type]: block,
        ...this.blocks,
      };
    });
    this.questInfo.adjustChildren();
    this.turnsInfo.adjustChildren();
  }

  makeTurn() {
    this.turns -= 1;
    this.update();
  }

  collectBlocks(typeID: string, count: number) {
    const block = this.blocks[typeID];

    if (!block) {
      return;
    }

    if (block.count > 0) {
      block.count -= count;
      block.label.text = block.count ? `x ${block.count}` : '  ✔';
      if (block.count === 0) {
        block.label.style.fill = '#00FF00';
      }
    }
  }

  checkGameover() {
    return this.turns <= 0;
  }

  checkCompleted() {
    const values = Object.values(this.blocks);

    return values.reduce<any>((acc, item) => {
      if (item.count > 0) {
        return false;
      }
      return acc;
    }, true);
  }

  update() {
    this.turnsLabel.text = this.turns.toString();
    this.turnsInfo?.adjustChildren();
  }
}

export default QuestPanel;
