import gsap from 'gsap';
import {Container, Graphics} from 'pixi.js';

import {AbscractScene, VisibleParams} from '~/types/scenes';
import {Colors} from '~/dictionary';

class EmptyScene extends AbscractScene {
  container: Container;
  debugContainer: Container;
  shutter: Graphics;

  constructor(screenWidth: number, screenHeight: number) {
    super();
    this.container = new Container();
    this.debugContainer = new Container();

    // Absolutely dark screen to overflow other group
    this.shutter = new Graphics();
    this.shutter.beginFill(Colors.Black, 1);
    this.shutter.drawRect(0, 0, screenWidth, screenHeight);
    this.shutter.endFill();

    this.container.addChild(this.shutter);
    this.container.addChild(this.debugContainer);
  }

  show({duration}: VisibleParams) {
    return new Promise<void>((onComplete) => {
      // Hide shutter by alpha animation
      gsap.to(this.shutter, {duration, alpha: 0, onComplete});
    });
  }

  hide() {
    return new Promise<void>((onComplete) => {
      gsap.to(this.shutter, {
        duration: 0.25,
        alpha: 1,
        onComplete,
      });
    });
  }
}

export default EmptyScene;
