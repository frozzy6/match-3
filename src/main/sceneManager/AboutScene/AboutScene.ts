import {Sprite, Container, Graphics, Text} from 'pixi.js';

import {Colors} from '~/dictionary';
import {AssetsResult} from '~/types/assets';

import EmptyScene from '../EmptyScene';

class GameScene extends EmptyScene {
  droped: boolean;
  assets: AssetsResult;
  screen: Container;
  screenWidth: number;
  screenHeight: number;
  levelText: Text;

  constructor(assets: AssetsResult, width: number, height: number) {
    super(width, height);
    this.assets = assets;
    this.screenWidth = width;
    this.screenHeight = height;
    this.droped = false;

    // Put it before shutter
    this.screen = new Container();
    this.container.addChildAt(this.screen, 0);

    const backgroundImage = Sprite.from(assets.LANDSCAPE.texture);
    const backgroundHeight = Math.max(height, 600);
    backgroundImage.height = backgroundHeight;
    backgroundImage.width = backgroundHeight * 1.4;
    backgroundImage.anchor.set(1, 0);
    backgroundImage.x = this.screenWidth;
    this.screen.addChild(backgroundImage);

    this.levelText = new Text('', {
      fontFamily: 'Knewave',
      fontSize: '22px',
      fill: Colors.LightGrey,
    });
    this.levelText.anchor.set(0.5, 1);
    this.levelText.position.x = this.screenWidth / 2;
    this.levelText.position.y = this.screenHeight - 8;

    this.screen.addChild(this.levelText);

    // Area to add jewels
    const gameArea = new Container();

    /* background */
    const background = new Graphics();
    this.screen.addChild(background);
    this.screen.addChild(gameArea);

    /* draw top tplate */
    const topMenu = new Graphics();
    this.screen.addChild(topMenu);
  }

  update() {
    //
  }
}

export default GameScene;
