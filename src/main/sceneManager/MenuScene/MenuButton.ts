import * as PIXI from 'pixi.js';

import Button from '~/main/accessory/Button';

const OFFSET = 30;
const BUTTON_WIDTH = 200;
const BUTTON_HEIGHT = BUTTON_WIDTH / 2.24;
const TEXT_OPTIONS = {
  fontFamily: 'Knewave',
  fill: '#3f3643',
  dropShadow: true,
  dropShadowColor: '#bababa',
  dropShadowDistance: 2,
  letterSpacing: 1,
};

const GAP = 8;

class MenuButton extends Button {
  buttonTexture: any;
  buttonTexturePressed: any;
  texture: any;
  text: any;
  onClickCallback: any;

  constructor({width, height, visible, orderNumber, label, assets, onClick}: any) {
    const x = width / 2 - BUTTON_WIDTH / 2;
    const y = height * 0.1 + (BUTTON_HEIGHT + OFFSET) * orderNumber;
    super(x, y);

    this.buttonTexture = assets.MENU_BUTTON.texture;
    this.buttonTexturePressed = assets.MENU_BUTTON_PRESSED.texture;

    this.texture = new PIXI.Sprite(this.buttonTexture);
    this.texture.buttonMode = true;
    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;

    this.graphics.visible = visible;
    this.graphics.addChild(this.texture);

    this.text = new PIXI.Text(label, TEXT_OPTIONS);
    this.text.anchor.set(0.5, 0.5);
    this.text.position.x = this.graphics.width / 2;
    this.text.position.y = this.graphics.height / 2.5;

    this.graphics.addChild(this.text);
    this.onClickCallback = onClick;
  }

  onMouseUp(e: any) {
    if (!this.isDown) return;
    this.text.position.y -= GAP;
    this.texture.texture = this.buttonTexture;
    this.onClickCallback();
    super.onMouseUp(e);
  }

  onMouseDown(e: any) {
    this.text.position.y += GAP;
    this.texture.texture = this.buttonTexturePressed;

    super.onMouseDown(e);
  }

  resize(width: any, height: any, order: any) {
    const x = width / 2 - BUTTON_WIDTH / 2;
    const y = height * 0.1 + (BUTTON_HEIGHT + OFFSET) * order;

    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;
    this.graphics.position.x = x;
    this.graphics.position.y = y;
  }

  onSpriteOut() {
    this.text.position.y -= GAP;
    this.texture.texture = this.buttonTexture;
    super.onSpriteOut();
  }
}

export default MenuButton;
