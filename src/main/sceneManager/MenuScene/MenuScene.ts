import {Sprite, Container, Graphics} from 'pixi.js';
import gsap, {Power2} from 'gsap';

import MenuButton from '~/main/sceneManager/MenuScene/MenuButton';
import {rand} from '~/utils/common';
import {Colors} from '~/dictionary';
import event, {Events} from '~/utils/event';
import {AssetsResult, Asset} from '~/types/assets';

import EmptyScene from '../EmptyScene';
import WindEmmiter from './WindEmmiter';
import Cloud from './Cloud';

function updateSizeAndPosition(sprite: any, width: any, height: any) {
  // eslint-disable-next-line no-param-reassign
  sprite.height = height;
  // eslint-disable-next-line no-param-reassign
  sprite.width = sprite.height / 0.75;
  // eslint-disable-next-line no-param-reassign
  sprite.x = width / 2;
}

class MenusScene extends EmptyScene {
  assets: AssetsResult;
  screenWidth: number;
  screenHeight: number;
  screen: Container;
  buttons: MenuButton[];
  clouds: Cloud[];
  static: {
    [key: string]: Sprite;
  };
  windEmmiter: any;
  shutters: any;
  elapsed: any;

  constructor(assets: AssetsResult, screenWidth: number, screenHeight: number) {
    super(screenWidth, screenHeight);
    this.assets = assets;
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;

    // Create container for this scene
    this.screen = new Container();
    const {screen} = this;
    // Put it before shutter
    this.container.addChildAt(screen, 0);
    this.buttons = [];
    this.clouds = [];
    this.static = {};
    // Create background image
    this.static.background = this.createStaticItem(Asset.START_MENU_BACKGROUND);

    const particlesContainer = new Container();
    screen.addChild(particlesContainer);

    this.windEmmiter = new WindEmmiter(particlesContainer, assets.WIND_PARTICLE.texture);
    this.createClouds();

    this.static.backTrees = this.createStaticItem(Asset.START_MENU_TREE);
    this.static.frontTrees = this.createStaticItem(Asset.START_MENU_TREES);
    this.static.ground = this.createStaticItem(Asset.START_MENU_GROUND);

    this.shutters = {
      borderLeft: new Graphics(),
      borderRight: new Graphics(),
    };
    screen.addChild(this.shutters.borderLeft);
    screen.addChild(this.shutters.borderRight);
    this.redrawShutterBorder();

    /**
     * Define menu buttons data
     */
    const BUTTONS_DATA = [
      {
        text: 'Continue',
        visible: false,
        onClick: () => {
          event.trigger(Events.Continue);
        },
      },
      {
        text: 'New Game',
        visible: true,
        onClick: () => {
          event.trigger(Events.LevelsSceneShow);
        },
      },
      // {
      //   text: 'About',
      //   visible: true,
      //   onClick: () => {
      //     gsap.to(this.buttons[0].graphics, {duration: 1, y: '-=500', ease: Power2.easeIn});
      //     gsap.to(this.buttons[1].graphics, {duration: 1.2, y: '-=700', ease: Power2.easeIn});
      //     gsap.to(this.buttons[2].graphics, {duration: 1.5, y: '-=900', ease: Power2.easeIn});
      //   },
      // },
    ];
    event.on(Events.StartNewGame, () => {
      setTimeout(() => {
        this.buttons[0].graphics.visible = true;
      }, 500);
    });

    event.on(Events.GameOver, () => {
      setTimeout(() => {
        this.buttons[0].graphics.visible = false;
      }, 500);
    });

    /**
     * Create menu buttons
     */
    for (let i = 0; i < BUTTONS_DATA.length; ++i) {
      const button = new MenuButton({
        label: BUTTONS_DATA[i].text,
        visible: BUTTONS_DATA[i].visible,
        onClick: BUTTONS_DATA[i].onClick,
        orderNumber: i,
        width: screenWidth,
        height: screenHeight,
        assets,
      });
      screen.addChild(button.graphics);
      this.buttons.push(button);
    }

    this.elapsed = new Date();
  }

  createStaticItem(image: Asset) {
    const sprite = Sprite.from(image);
    sprite.anchor.set(0.5, 0);
    updateSizeAndPosition(sprite, this.screenWidth, this.screenHeight);
    this.screen.addChild(sprite);

    return sprite;
  }

  createClouds() {
    const CLOUD_TYPES = 5;

    for (let i = 0; i < 5; i++) {
      const cloudId = rand(CLOUD_TYPES);

      const cloud = new Cloud(
        this.assets[`CLOUD_${cloudId}` as Asset].texture,
        this.screenWidth,
        this.screenHeight
      );
      this.screen.addChild(cloud);
      this.clouds.push(cloud);
    }
  }

  async show() {
    super.show({duration: 0.3});
  }

  update() {
    const now = new Date();
    this.windEmmiter.update((now as any) - this.elapsed);
    this.clouds.forEach(({update}: any) => update());

    this.elapsed = now;
  }

  redrawShutterBorder() {
    const {width: bgWidth} = this.static.background;
    const {borderLeft, borderRight} = this.shutters;

    if (this.screenWidth <= bgWidth) {
      borderLeft.clear();
      borderRight.clear();
      return false;
    }

    const delta = this.screenWidth - bgWidth;
    borderLeft.clear();
    borderLeft.beginFill(Colors.Black);
    borderLeft.drawRect(0, 0, delta / 2, this.screenHeight);
    borderLeft.endFill();

    borderRight.clear();
    borderRight.beginFill(Colors.Black);
    borderRight.drawRect(delta / 2 + bgWidth, 0, delta / 2, this.screenHeight);
    borderRight.endFill();
  }

  resize(screenWidth: number, screenHeight: number) {
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;

    this.buttons.forEach((btn, index) => btn.resize(screenWidth, screenHeight, index));
    // eslint-disable-next-line no-restricted-syntax, guard-for-in
    for (const key in this.static) {
      updateSizeAndPosition(this.static[key], screenWidth, screenHeight);
    }

    this.redrawShutterBorder();
  }
}

export default MenusScene;
