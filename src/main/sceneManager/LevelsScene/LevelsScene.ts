import {Sprite, Container, Text} from 'pixi.js';

import event, {Events} from '~/utils/event';
import {AssetsResult, Asset} from '~/types/assets';

import EmptyScene from '../EmptyScene';
import LevelButton from './parts/LevelButton';

const TEXT_OPTIONS = {
  fontFamily: 'Knewave',
  fill: '#3f3643',
  dropShadow: true,
  fontSize: 40,
  dropShadowColor: '#bababa',
  dropShadowDistance: 2,
  letterSpacing: 1,
};

class LevelsScene extends EmptyScene {
  assets: AssetsResult;
  screenWidth: number;
  screenHeight: number;
  screen: Container;
  graphics: {
    [key: string]: Sprite | Text;
  };
  buttons: LevelButton[];

  constructor(assets: AssetsResult, screenWidth: number, screenHeight: number) {
    super(screenWidth, screenHeight);
    this.assets = assets;
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
    // Create container for this scene
    this.screen = new Container();
    const {screen} = this;
    // Put it before shutter
    this.container.addChildAt(screen, 0);

    this.graphics = {};
    const midX = screenWidth / 2;

    const sprite = Sprite.from(Asset.LANDSCAPE);
    sprite.anchor.set(0.5, 0);
    sprite.position.x = midX;
    this.screen.addChild(sprite);

    this.graphics.background = sprite;

    const text = new Text('Levels', TEXT_OPTIONS);
    text.anchor.set(0.5, 0.5);
    text.position.x = midX;
    text.position.y = 50;
    this.container.addChild(text);

    const BUTTONS_DATA = [
      {
        x: midX - 120,
        y: 120,
        label: '1',
        onClick: () => event.trigger(Events.StartNewGame, {level: 1}),
        stars: 0,
      },
      {
        x: midX,
        y: 120,
        label: '2',
        onClick: () => event.trigger(Events.StartNewGame, {level: 2}),
        stars: 0,
      },
      {
        x: midX + 120,
        y: 120,
        label: '3',
        onClick: () => event.trigger(Events.StartNewGame, {level: 3}),
        stars: 0,
      },
    ];

    this.buttons = BUTTONS_DATA.map((data) => new LevelButton(data, this.container));
  }

  resize(screenWidth: number, screenHeight: number) {
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
  }
}

export default LevelsScene;
