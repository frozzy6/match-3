import {Sprite, Texture, Text, filters} from 'pixi.js';

import Button from '~/main/accessory/Button';

const BUTTON_WIDTH = 100;
const BUTTON_HEIGHT = BUTTON_WIDTH;

const TEXT_OPTIONS = {
  fontFamily: 'Knewave',
  fill: '#3f3643',
  dropShadow: true,
  dropShadowColor: '#bababa',
  dropShadowDistance: 2,
  letterSpacing: 1,
};

class LevelButton extends Button {
  buttonTexture: Texture;
  buttonTexturePressed: Texture;
  sprite: Sprite;
  text: Text;
  star: Sprite;
  star2: Sprite;
  star3: Sprite;
  onClickCallback: () => void;

  constructor({x, y, label, onClick, stars}: any, container: any) {
    super(x, y);

    this.buttonTexture = Texture.from('images/button-small.png');
    this.buttonTexturePressed = Texture.from('images/button-small-pressed.png');

    this.sprite = new Sprite(this.buttonTexture);
    this.sprite.anchor.set(0.5, 0);
    this.sprite.buttonMode = true;
    this.sprite.width = BUTTON_WIDTH;
    this.sprite.height = BUTTON_HEIGHT;

    this.graphics.addChild(this.sprite);

    this.text = new Text(label, TEXT_OPTIONS);
    this.text.anchor.set(0.5, 0.5);
    // this.text.position.x = x;
    this.text.position.y = BUTTON_HEIGHT / 2 - 5;
    this.graphics.addChild(this.text);

    const f = new filters.ColorMatrixFilter();
    f.greyscale(0.2, false);

    this.star = Sprite.from('images/ico/star.png');
    this.star.anchor.set(0.5, 0.5);
    this.star.scale.set(0.5, 0.5);
    this.star.rotation = -0.5;
    this.star.position.set(-(BUTTON_WIDTH / 2.7), 5);
    this.star.filters = [f];
    this.graphics.addChild(this.star);

    this.star2 = Sprite.from('images/ico/star.png');
    this.star2.anchor.set(0.5, 0.5);
    this.star2.scale.set(0.5, 0.5);
    this.star2.position.set(0, 0);
    this.star2.filters = [f];
    this.graphics.addChild(this.star2);

    this.star3 = Sprite.from('images/ico/star.png');
    this.star3.anchor.set(0.5, 0.5);
    this.star3.position.set(BUTTON_WIDTH / 2.7, 5);
    this.star3.scale.set(0.5, 0.5);
    this.star3.rotation = 0.5;
    this.star3.filters = [f];
    this.graphics.addChild(this.star3);

    if (stars >= 1) {
      this.star.filters = [];
    }
    if (stars >= 2) {
      this.star2.filters = [];
    }
    if (stars >= 3) {
      this.star3.filters = [];
    }

    this.onClickCallback = onClick;

    if (container) {
      container.addChild(this.graphics);
    }
  }

  onMouseUp(e: any) {
    if (!this.isDown) return;
    this.text.position.y -= 4;
    this.star.position.y -= 4;
    this.star2.position.y -= 4;
    this.star3.position.y -= 4;
    this.sprite.texture = this.buttonTexture;
    this.onClickCallback();
    super.onMouseUp(e);
  }

  onMouseDown(e: any) {
    this.text.position.y += 4;
    this.star.position.y += 4;
    this.star2.position.y += 4;
    this.star3.position.y += 4;
    this.sprite.texture = this.buttonTexturePressed;

    super.onMouseDown(e);
  }

  resize(width: any, height: any, order: any) {
    console.log('not implemented resize', width, height, order);
  }

  onSpriteOut() {
    this.sprite.texture = this.buttonTexture;
    this.text.position.y -= 4;
    this.star.position.y -= 4;
    this.star2.position.y -= 4;
    this.star3.position.y -= 4;
    super.onSpriteOut();
  }
}

export default LevelButton;
