import {Container, Text} from 'pixi.js';

import Info from '~/main/common/Info';

const TEXT_OPTIONS = {
  fontSize: '22px',
  fontFamily: 'AppleTea',
  fill: 'white',
};

class Score {
  score: number;
  container: Container;
  scoreText: Text;
  info: Info;

  constructor(screen: Container, x: number, y: number) {
    this.score = 0;
    this.container = screen;

    this.scoreText = new Text(this.score.toString(), TEXT_OPTIONS);

    this.info = new Info({
      screen,
      width: 80,
      height: 50,
      x,
      y,
      label: 'score',
      children: this.scoreText,
    });
  }

  addValue(v: number) {
    this.score += v;
    this.scoreText.text = this.score.toString();
    this.info.adjustChildren();
  }

  reset() {
    this.score = 0;
    this.scoreText.text = this.score.toString();
    this.info.adjustChildren();
  }
}

export default Score;
