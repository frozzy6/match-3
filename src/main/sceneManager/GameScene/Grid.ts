import gsap from 'gsap';

import {GameLevel} from '~/types/assets';
import {TPoint} from '~/types/common';
import {rand} from '~/utils/common';

import Jewel from './Jewel';

class Grid {
  options: any;
  grid: Array<Array<Jewel | null>>;
  types: number;
  maxX: number;
  maxY: number;

  constructor(options: any) {
    this.options = options;
    this.types = 0;
    this.grid = [];

    this.maxX = 0;
    this.maxY = 0;
  }

  forEach(cb: any) {
    return this.grid.forEach(cb);
  }

  getItem(x: any, y: any) {
    return this.grid[x][y];
  }

  removeItem(x: any, y: any) {
    this.grid[x][y] = null;
  }

  removeItems = (items: any) => items.forEach(({x, y}: any) => this.removeItem(x, y));

  swap(p1: {x: number; y: number}, p2: {x: number; y: number}) {
    const temp = this.grid[p1.x][p1.y];
    this.grid[p1.x][p1.y] = this.grid[p2.x][p2.y];
    this.grid[p2.x][p2.y] = temp;
  }

  isNeighbours(j1: any, j2: any) {
    const coeff = Math.abs(j1.x - j2.x) + Math.abs(j1.y - j2.y);
    return coeff === 1;
  }

  getWorldCoords(jewel: any) {
    for (let x = 0; x < this.grid.length; ++x) {
      const row = this.grid[x];
      const y = row.indexOf(jewel);
      if (y > -1) {
        return {x, y};
      }
    }
    return null;
  }

  findMathesFor(x: any, y: any) {
    const jewel = this.grid[x][y];
    if (!jewel) {
      console.warn('Call find matches to empty block');
      return [];
    }

    if (jewel.typeID === 'x') {
      return [];
    }
    const {typeID} = jewel;
    const checkedX = [{x, y}];
    const checkedY = [{x, y}];

    // <---
    for (let i = x - 1; i >= 0; i--) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({x: i, y});
    }

    // --->
    for (let i = x + 1; i < this.grid.length; i++) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({x: i, y});
    }

    // ↑
    for (let i = y - 1; i >= 0; i--) {
      const compareJewel = this.grid[x][i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({x, y: i});
    }

    // ↓
    for (let i = y + 1; i < this.grid[x].length; i++) {
      const compareJewel = this.grid[x][i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({x, y: i});
    }

    if (checkedX.length >= 3 && checkedY.length >= 3) {
      return checkedX.concat(checkedY);
    }

    if (checkedY.length >= 3) {
      return checkedY;
    }

    if (checkedX.length >= 3) {
      return checkedX;
    }

    return [];
  }

  checkRightPlacement(x: any, y: any, typeID: any, column: any) {
    const checkedX = [{x, y}];
    const checkedY = [{x, y}];

    // <---
    for (let i = x - 1; i >= Math.max(0, x - 2); i--) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({x: i, y});
    }

    // ↑
    for (let i = y - 1; i >= Math.max(0, y - 2); i--) {
      const compareJewel = column[i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({x, y: i});
    }

    if (checkedX.length >= 3 && checkedY.length >= 3) {
      return checkedX.concat(checkedY);
    }

    if (checkedY.length >= 3) {
      return checkedY;
    }

    if (checkedX.length >= 3) {
      return checkedX;
    }

    return false;
  }

  checkAvailbleSwipes() {
    // const checkVerticalMatching = (typeID: any, x: any, y: any, swipedJewel: any = {}) => {
    //   let count = 0;
    //   for (let i = y + 1; i < this.height; i++) {
    //     const jewelType = swipedJewel.y === i ? swipedJewel.typeID : this.grid[x][i]?.typeID;
    //     if (jewelType === typeID) {
    //       count++;
    //     } else {
    //       break;
    //     }
    //   }

    //   for (let i = y - 1; i >= 0; i--) {
    //     const jewelType = swipedJewel.y === i ? swipedJewel.typeID : this.grid[x][i]?.typeID;
    //     if (jewelType === typeID) {
    //       count++;
    //     } else {
    //       break;
    //     }
    //   }

    //   return count >= 2;
    // };

    // const checkHorizontalMatching = (typeID: any, x: any, y: any, swipedJewel: any = {}) => {
    //   let count = 0;
    //   for (let i = x + 1; i < this.width; i++) {
    //     const jewelType = swipedJewel.x === i ? swipedJewel.typeID : this.grid[i][y]?.typeID;
    //     if (jewelType === typeID) {
    //       count++;
    //     } else {
    //       break;
    //     }
    //   }

    //   for (let i = x - 1; i >= 0; i--) {
    //     const jewelType = swipedJewel.x === i ? swipedJewel.typeID : this.grid[i][y]?.typeID;
    //     if (jewelType === typeID) {
    //       count++;
    //     } else {
    //       break;
    //     }
    //   }

    //   return count >= 2;
    // };

    // const checkSwipesForJewels = (jewel: any, x: any, y: any) => {
    //   const result: any = [];
    //   if (!jewel) {
    //     return result;
    //   }
    //   const {typeID} = jewel;

    //   // Process right swiping
    //   if (x + 1 < this.width) {
    //     const leftTypeID = this.grid[x + 1][y]?.typeID;
    //     if (
    //       leftTypeID &&
    //       checkVerticalMatching(leftTypeID, x, y) ||
    //       checkHorizontalMatching(leftTypeID, x, y, {typeID, x: x + 1})
    //     ) {
    //       result.push({from: {x: x + 1, y}, to: {x, y}});
    //     }

    //     if (
    //       leftTypeID &&
    //       checkVerticalMatching(typeID, x + 1, y) ||
    //       checkHorizontalMatching(typeID, x + 1, y, {typeID: leftTypeID, x})
    //     ) {
    //       result.push({from: {x, y}, to: {x: x + 1, y}});
    //     }
    //   }

    //   // Process bottom swiping
    //   if (y + 1 < this.height) {
    //     const leftTypeID = this.grid[x][y + 1]?.typeID;

    //     // Check left bottom
    //     if (
    //       leftTypeID &&
    //       checkVerticalMatching(leftTypeID, x, y, {typeID, y: y + 1}) ||
    //       checkHorizontalMatching(leftTypeID, x, y)
    //     ) {
    //       result.push({from: {x, y: y + 1}, to: {x, y}});
    //     }
    //     if (
    //       leftTypeID &&
    //       checkVerticalMatching(typeID, x, y + 1, {typeID: leftTypeID, y}) ||
    //       checkHorizontalMatching(typeID, x, y + 1)
    //     ) {
    //       result.push({from: {x, y}, to: {x, y: y + 1}});
    //     }
    //   }

    //   return result;
    // };

    // const matches: any = [];
    // this.grid.forEach((column: any, x: any) => {
    //   column.forEach((item: any, y: any) => {
    //     const match = checkSwipesForJewels(item, x, y);
    //     if (match.length > 0) {
    //       matches.push(match);
    //     }
    //   });
    // });

    // return matches.flat();
    return [];
  }

  generate() {
    console.log('not iomplemented');
    // for (let x = 0; x < this.width; x++) {
    //   const column = [];
    //   for (let y = 0; y < this.height; y++) {
    //     let typeID = rand(this.types);
    //     let attempts = 0;
    //     do {
    //       const needToDrop = this.checkRightPlacement(x, y, typeID, column);
    //       if (!needToDrop) {
    //         break;
    //       }
    //       attempts++;
    //       typeID = rand(this.types);
    //     } while (attempts < 100);

    //     const jewel = drawCallback(typeID, x, y);
    //     column.push(jewel);
    //   }
    //   this.grid.push(column);
    // }
  }

  createJewel(typeId: string, x: number, y: number) {
    const {
      handlers,
      container,
      jewel: {size, gap, offset},
    } = this.options;

    const jewel = new Jewel({
      id: typeId,
      x: x * (gap + size) + offset,
      y: y * (gap + size) + offset,
      width: size,
      height: size,
    });

    jewel.sprite.on('pointerdown', () => {
      handlers.jewelMouseDown(jewel);
    });

    jewel.sprite.on('pointerup', () => {
      handlers.jewelMouseUp(jewel);
    });

    jewel.sprite.on('touchend', () => {
      handlers.jewelMouseIn(jewel);
    });

    // jewel.sprite.on('touchend', () => {
    //   console.log('out');
    // });

    container.addChild(jewel.sprite);

    return jewel;
  }

  loadLevel(level: GameLevel) {
    let maxX = 0;
    let maxY = 0;
    this.grid = level.grid.map((column: any, x: number) => {
      maxX = Math.max(x, maxX);
      return column.map((item: any, y: number) => {
        maxY = Math.max(y, maxY);
        return this.createJewel(item, x, y);
      });
    });
    this.types = level.jewels;
    console.log(`loading ${level.name} done, x: ${maxX}, y: ${maxY}`);
    this.maxX = maxX;
    this.maxY = maxY;

    return {level, maxX: maxX + 1, maxY: maxY + 1};
  }

  refillColumn = async (x: number) => {
    const column = this.grid[x];
    const hasEmptyValues = column.some((jewel: Jewel | null) => jewel === null);
    const recheckPoints: TPoint[] = [];

    if (!hasEmptyValues) {
      return recheckPoints;
    }

    // Add recheck poinst from latest null index to 0
    const latestNullPosition = column.lastIndexOf(null);
    for (let y = latestNullPosition; y >= 0; y--) {
      recheckPoints.push({x, y});
    }

    let moveDown = false;
    let emptyCounter = 0;
    let xCounter = 0;

    const refilePromise = new Promise((onComplete) => {
      const timeline = gsap.timeline({onComplete});

      for (let y = column.length - 1; y >= 0; y--) {
        const jewel: Jewel | null = column[y];
        if (jewel && moveDown && jewel.typeID !== 'x') {
          this.grid[x][y] = null;
          this.grid[x][y + emptyCounter + xCounter] = jewel;
          jewel.moveDownTo({
            timeline,
            times: emptyCounter + xCounter,
            offset: this.options.jewel.gap,
          });
          xCounter = 0;
        }

        if (!jewel) {
          moveDown = true;
          emptyCounter++;
        } else if (jewel.typeID === 'x') {
          xCounter++;
        }
      }

      for (let y = column.length - 1; y >= 0; y--) {
        const jewel = column[y];
        if (!jewel) {
          const newJewel = this.createJewel(rand(this.types).toString(), x, -1);

          column[y] = newJewel;

          newJewel.moveDownTo({times: y + 1, timeline, offset: this.options.jewel.gap});
        }
      }
    });

    await refilePromise;

    return recheckPoints;
  };

  refillColumns() {
    return this.grid.map((column: any, x: any) => this.refillColumn(x));
  }
}

export default Grid;
