import {Container, Text, Graphics} from 'pixi.js';
import gsap from 'gsap';

import {Colors} from '~/dictionary';
import event, {Events} from '~/utils/event';
import {AssetsResult} from '~/types/assets';

import MenuButton from '../../MenuScene/MenuButton';

const TEXT_OPTIONS = {
  fontSize: '44px',
  fontFamily: 'Knewave',
  fill: '#CA3433',
};

class GameOver {
  screen: Container;
  container: Container;
  background: Graphics;
  label: Text;
  timeline: GSAPTimeline;
  screenWidth: number;
  screenHeight: number;
  backButton: MenuButton;

  constructor(assets: AssetsResult, screen: Container, screenWidth: number, screenHeight: number) {
    this.screen = screen;
    this.container = new Container();
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;

    this.timeline = gsap.timeline();

    this.background = new Graphics();
    this.background.beginFill(Colors.Black);
    this.background.drawRect(0, 0, 5, 5);
    this.background.endFill();
    this.background.position.set(screenWidth / 2, screenHeight / 2);

    this.container.addChild(this.background);

    this.label = new Text('Game Over', TEXT_OPTIONS);
    this.label.anchor.set(0.5, 0.5);
    this.label.position.set(screenWidth / 2, screenHeight / 2);
    this.container.addChild(this.label);

    // TODO: rework menu buttons. pass x and y to constructor
    this.backButton = new MenuButton({
      label: 'Menu',
      visible: false,
      onClick: () => {
        event.trigger(Events.BackToMenu);
      },
      width: screenWidth,
      height: screenHeight,
      orderNumber: 4,
      assets,
    });

    this.container.addChild(this.backButton.graphics);
    screen.addChild(this.container);
    this.container.alpha = 0;
    this.label.alpha = 0;
  }

  show() {
    const STEP_DURATION = 0.3;
    const resultHeight = 300;
    const tl = this.timeline;

    tl.to(this.background.position, {
      duration: STEP_DURATION,
      y: this.screenHeight / 2 - resultHeight / 2,
    });
    tl.to(
      this.background,
      {
        duration: STEP_DURATION,
        height: resultHeight,
      },
      0
    );
    tl.to(
      this.container,
      {
        duration: STEP_DURATION,
        alpha: 1,
      },
      0
    );
    tl.to(this.background, {
      duration: STEP_DURATION * 2,
      width: this.screenWidth,
    });
    tl.to(
      this.background.position,
      {
        duration: STEP_DURATION * 2,
        x: 0,
      },
      '<'
    );

    tl.to(this.label, {
      duration: STEP_DURATION,
      alpha: 1,
    });

    tl.to(this.background.position, {
      duration: 1,
      y: 0,
    });
    tl.to(
      this.background,
      {
        duration: 1,
        height: this.screenHeight,
        onComplete: () => {
          this.backButton.graphics.visible = true;
        },
      },
      '<'
    );

    // this.turnsLabel.text = this.turns.toString();
  }

  destroy() {
    this.screen.removeChild(this.container);
  }
}

export default GameOver;
