import {Sprite, Container, Graphics, Text, Texture} from 'pixi.js';

import event, {Events} from '~/utils/event';
import {Colors} from '~/dictionary';
import {Asset, AssetsResult, GameLevel} from '~/types/assets';
import QuestPanel from '~/main/common/QuestPanel';

import EmptyScene from '../EmptyScene';
import JewelsGame from './JewelsGame';
import HomeButton from './HomeButton';
import HelpButton from './HelpButton';
import Score from './Score';
import {TGameOptions} from './types';
import GameOver from './parts/GameOver';
import CompleteLevel from './parts/CompleteLevel';

const SIDES_GAP = 80;
const GAME_FIELD_TOP_MARGIN = 200;
const MAX_SCREEN_WIDTH = 500;

interface GameGraphics {
  backgroundImage: Sprite;
  homeBtn: HomeButton;
  helpBtn: HelpButton;
  gameArea: Container;
  background: Graphics;
  topMenu: Graphics;
}

class GameScene extends EmptyScene {
  droped: boolean;
  assets: AssetsResult;
  screen: Container;
  screenWidth: number;
  screenHeight: number;
  gameOptions: TGameOptions;
  activeGame?: JewelsGame;
  score: Score;
  questPanel: QuestPanel;
  graphics: GameGraphics;
  levelText: Text;
  gameOver: GameOver | null;
  completeLevel: CompleteLevel | null;

  constructor(assets: AssetsResult, w: number, height: number, options: any) {
    const width = Math.min(w, MAX_SCREEN_WIDTH);

    super(width, height);
    this.assets = assets;
    this.screenWidth = width;
    this.screenHeight = height;
    this.droped = false;
    this.gameOptions = options || {};

    // Put it before shutter
    this.screen = new Container();
    this.container.addChildAt(this.screen, 0);

    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;

    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

    const grd = ctx.createLinearGradient(0, 0, 0, height);
    grd.addColorStop(0, '#66d6da');
    grd.addColorStop(0.5, '#7ee6e9');
    grd.addColorStop(1, '#befcfd');

    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, width, height);

    const texture = Texture.from(canvas);

    const backgroundImage = Sprite.from(texture);

    backgroundImage.height = height;
    backgroundImage.width = width;
    this.screen.addChild(backgroundImage);

    /* draw top tplate */
    const topMenu = new Graphics();
    this.screen.addChild(topMenu);

    const homeBtn = new HomeButton(0, 10);
    homeBtn.onClick = () => (event as any).trigger(Events.BackToMenu);
    homeBtn.graphics.visible = false;

    const helpBtn = new HelpButton(0, 10);
    helpBtn.onClick = () => this.activeGame?.toggleDebug();

    this.score = new Score(this.screen, 20, 20);
    this.questPanel = new QuestPanel(this.screen, 110, 20);

    this.levelText = new Text('', {
      fontFamily: 'Knewave',
      fontSize: '22px',
      fill: Colors.LightGrey,
    });
    this.levelText.anchor.set(0.5, 1);
    this.levelText.position.x = this.screenWidth / 2;
    this.levelText.position.y = this.screenHeight - 8;

    this.screen.addChild(this.levelText);

    // Area to add jewels
    const gameArea = new Container();
    gameArea.position.y = GAME_FIELD_TOP_MARGIN;
    gameArea.width = this.screenWidth - SIDES_GAP;
    gameArea.position.x = SIDES_GAP / 2;

    /* background */
    const background = new Graphics();
    this.screen.addChild(background);
    this.screen.addChild(gameArea);

    this.graphics = {
      backgroundImage,
      homeBtn,
      helpBtn,
      gameArea,
      background,
      topMenu,
    };

    this.updateGameScreen();

    this.screen.addChild(homeBtn.graphics);
    this.gameOver = null;
    this.completeLevel = null;

    event.on(Events.StartNewGame, this.resetGame);
    event.on(Events.AddScore, ({counter, jewelsCount}: any) => {
      this.score.addValue(counter * jewelsCount);
    });

    this.screen.position.x = w / 2 - this.screen.width / 2;
  }

  updateGameScreen() {
    const {background, topMenu, gameArea, homeBtn, helpBtn} = this.graphics;
    const gameAreaWidth = this.screenWidth - SIDES_GAP;

    homeBtn.setPosition(this.screenWidth - 70, 10);
    helpBtn.setPosition(this.screenWidth - 120, 10);

    gameArea.position.x = this.screenWidth / 2 - gameAreaWidth / 2;

    /* draw top tplate */
    topMenu.clear();
    topMenu.beginFill(0x000000, 0.6);
    topMenu.drawRect(0, 0, this.screenWidth, 90);
    topMenu.endFill();
    topMenu.lineStyle(5, Colors.LightGrey);
    topMenu.moveTo(0, 90);
    topMenu.lineTo(this.screenWidth, 90);

    background.clear();
    background.beginFill(Colors.Black, 0.2);
    background.lineStyle(5, Colors.White);
    background.drawRoundedRect(
      gameArea.position.x - 5,
      gameArea.position.y - 5,
      gameAreaWidth + 10,
      (this.activeGame?.plates.height || 100) + 12,
      10
    );
    background.endFill();
  }

  resetGame = (params: any) => {
    const {assets} = this;

    if (this.activeGame) {
      this.activeGame.destroy();
      this.droped = false;
    }
    this.gameOver?.destroy();
    this.completeLevel?.destroy();
    this.gameOver = null;
    this.completeLevel = null;

    const level: GameLevel = assets[`LEVEL_${params.level}` as Asset].data;
    this.questPanel.setQuestForLevel(level.quest);

    this.activeGame = new JewelsGame({
      assets: this.assets,
      container: this.graphics.gameArea,
      screen: this.screen,
      screenWidth: this.screenWidth - SIDES_GAP,
      level: assets[`LEVEL_${params.level}` as Asset].data,
      onGameOver: this.onGameOver,
      onLevelCompleted: this.onLevelCompleted,
      quest: this.questPanel,
      ...this.gameOptions,
    });

    this.levelText.text = this.activeGame.level.name;

    this.score.reset();
    this.updateGameScreen();
  };

  async show(showTime: any) {
    super.show(showTime);
    if (!this.droped) {
      this.activeGame?.animateDrop();
      this.droped = true;
    }
  }

  onGameOver = () => {
    this.gameOver?.destroy();
    this.gameOver = new GameOver(this.assets, this.screen, this.screenWidth, this.screenHeight);
    this.gameOver.show();

    event.trigger(Events.GameOver);
  };

  onLevelCompleted = () => {
    this.completeLevel?.destroy();
    this.completeLevel = new CompleteLevel(
      this.assets,
      this.screen,
      this.screenWidth,
      this.screenHeight
    );
    this.completeLevel.show();

    event.trigger(Events.GameOver);
  };

  resize(screenWidth: any, screenHeight: any) {
    const width = Math.min(screenWidth, MAX_SCREEN_WIDTH);
    this.screenWidth = width;
    this.screenHeight = screenHeight;
    // const backgroundHeight = Math.max(height, 600);
    // const backgroundWidth = height * 1.4;
    // this.graphics.backgroundImage.height = backgroundHeight;
    // this.graphics.backgroundImage.width = backgroundWidth;
    // this.graphics.backgroundImage.x = width / 2;
    this.updateGameScreen();
    // this.score.setPosition(width / 2 - backgroundWidth / 2);
    // this.homeBtn.setPosition(width / 2 + backgroundWidth / 2 - 60, 10);
    // this.helpBtn.setPosition(width / 2 + backgroundWidth / 2 - 120, 10);
  }
}

export default GameScene;
