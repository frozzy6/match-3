export interface TGameOptions {
  container?: any;
  jewel: {
    size: number;
    gap: number;
    offset: number;
  };
}
