import {Container, Graphics, Sprite} from 'pixi.js';
import gsap, {Power0, Power2} from 'gsap';

import {Colors} from '~/dictionary';
import event, {Events} from '~/utils/event';
import QuestPanel from '~/main/common/QuestPanel';

import Grid from './Grid';

class JewelsGame {
  options: any;
  container: Container;
  gameScreen: Container;
  jewelsContainer: Container;
  plates: Container;
  debugContainer: Container;
  grid: Grid;
  maxX: number;
  maxY: number;
  _firstJewel: any;
  _secondJewel: any;
  _seeking: any;
  _disableInput: any;
  gameAreaMask: any;
  level: any;
  screenWidth: any;
  jewelOptions: any;
  quest: QuestPanel;

  constructor(options: any) {
    this.options = options;
    this.container = options.container;
    this.gameScreen = options.screen;
    this.screenWidth = options.screenWidth;
    this.quest = options.quest;

    this.plates = new Container();
    this.jewelsContainer = new Container();
    this.debugContainer = new Container();
    this.debugContainer.visible = false;

    // Game area mask makes jewels only visible on this rect
    this.gameAreaMask = new Graphics();

    this.container.addChild(this.plates);
    this.container.addChild(this.jewelsContainer);
    this.container.addChild(this.debugContainer);
    this.container.addChild(this.gameAreaMask);

    this.maxX = this.options.level.grid.length;
    this.maxY = this.options.level.grid[0].length;

    this.jewelOptions = this.getJewelsSize();
    const {size, gap, offset} = this.jewelOptions;

    this.grid = new Grid({
      ...options,
      container: this.jewelsContainer,
      handlers: {
        jewelMouseDown: this.jewelMouseDown,
        jewelMouseUp: this.jewelMouseUp,
        jewelMouseIn: this.jewelMouseIn,
        jewelMouseOut: this.jewelMouseOut,
      },
      jewel: this.jewelOptions,
    });
    const {maxX, maxY, level} = this.grid.loadLevel(options.level);

    this.level = level;

    this.gameAreaMask.clear();
    this.gameAreaMask.beginFill(Colors.Black, 0.5);
    this.gameAreaMask.drawRect(0, 0, (size + gap) * maxX + gap, (size + gap) * maxY + gap);
    this.gameAreaMask.endFill();
    this.container.mask = this.gameAreaMask;

    this.grid.forEach((column: any, x: any) => {
      column.forEach((jewel: any, y: any) => {
        if (jewel.typeID === 'x') {
          return;
        }
        const id = (x + y) % 2 === 0 ? 4 : 5;
        const sprite = Sprite.from(`images/ui/${id}.png`);
        sprite.anchor.set(0.5, 0.5);
        sprite.width = size + gap - 1;
        sprite.height = size + gap - 1;
        sprite.position.x = x * (gap + size) + offset;
        sprite.position.y = y * (gap + size) + offset;
        sprite.alpha = 0.85;

        this.plates.addChild(sprite);
      });
    });
    // this.grid.generate((typeId: any, x: any, y: any) => {
    //   const id = (x + y) % 2 === 0 ? 4 : 5;
    //   const sprite = Sprite.from(`images/ui/${id}.png`);
    //   sprite.anchor.x = 0.5;
    //   sprite.anchor.y = 0.5;
    //   sprite.width = size + gap - 1;
    //   sprite.height = size + gap - 1;
    //   sprite.position.x = x * (gap + size) + offset;
    //   sprite.position.y = y * (gap + size) + offset;
    //   sprite.alpha = 0.85;

    //   this.plates.addChild(sprite);
    //   if (x === 3 && y === 3) {
    //     return null;
    //   }
    //   return this.createJewel(typeId, x, y);
    // });

    const matches = this.grid.checkAvailbleSwipes();
    this.drawMatches(matches);

    this._firstJewel = null;
    this._secondJewel = null;
    this._seeking = false;
    this._disableInput = false;
  }

  getJewelsSize() {
    const fullGameAreaWidth = this.screenWidth;
    const jewelsCountX = this.maxX;

    const size = fullGameAreaWidth / (jewelsCountX * 1.5);
    const gap = size / 2;
    const offset = (gap + size) / 2;

    return {
      size,
      gap,
      offset,
    };
  }

  toggleDebug() {
    this.debugContainer.visible = !this.debugContainer.visible;
  }

  drawMatches(matches: any) {
    const {size, gap, offset} = this.jewelOptions;

    this.debugContainer.removeChildren();

    matches.forEach(({from, to}: any) => {
      const line = new Graphics();
      line.lineStyle(4, 0x000000, 1);
      line.moveTo(from.x * (size + gap) + offset, from.y * (size + gap) + offset);
      line.lineTo(to.x * (size + gap) + offset, to.y * (size + gap) + offset);
      this.debugContainer.addChild(line);
    });
  }

  destroy() {
    this.container.removeChildren();
  }

  animateDrop() {
    const {size, gap, offset} = this.jewelOptions;
    const masterTimeline = gsap.timeline();

    this.grid.forEach((column: any, x: any) => {
      const timeline = gsap.timeline();

      column.forEach((jewel: any, y: any) => {
        if (jewel.type === -1) {
          return false;
        }
        jewel.sprite.y = -((column.length - y) * size + (column.length - y) * gap + offset) - 10;

        timeline.to(
          jewel.sprite,
          {
            duration: 0.6,
            y: y * size + y * gap + offset,
            ease: Power0.easeNone,
          },
          0
        );
      });

      masterTimeline.add(timeline, x * 0.1);
    });
  }

  jewelMouseDown = (jewel: any) => {
    if (this._disableInput) {
      return false;
    }
    if (!this._firstJewel || this._firstJewel === jewel) {
      this._firstJewel = jewel;
      this._seeking = true;
    } else {
      this._secondJewel = jewel;
      this.checkSelected();
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  jewelMouseUp = (_jewel: any) => {
    this._seeking = false;
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  jewelMouseOut = (_jewel: any, _event: any) => {
    if (!this._firstJewel || this._disableInput) {
      return false;
    }
    this._seeking = true;
  };

  jewelMouseIn = (jewel: any) => {
    if (!this._seeking || this._disableInput) {
      return false;
    }

    this._secondJewel = jewel;
    this._seeking = false;
    this.checkSelected();
  };

  checkSelected() {
    let jFCoords: any = null;
    let jSCoords: any = null;

    // const gamePanel = this.container;
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self: any = this;

    const isNeighbours = (j1: any, j2: any) => {
      jFCoords = this.grid.getWorldCoords(j1);
      jSCoords = this.grid.getWorldCoords(j2);

      if (!jFCoords || !jSCoords) {
        return false;
      }
      return this.grid.isNeighbours(jFCoords, jSCoords);
    };

    const enableInput = () => {
      this._disableInput = false;
    };

    /**
     * Disable user input
     */
    this._disableInput = true;

    /**
     * If we have no target
     */
    if (!this._firstJewel || !this._secondJewel) {
      return false;
    }

    /**
     * If jewels of user choice are not neighbours
     */
    if (!isNeighbours(this._firstJewel, this._secondJewel)) {
      /**
       * Make second jewel are first and null second
       */
      this._firstJewel = this._secondJewel;
      this._secondJewel = null;

      /**
       * Enable input
       */
      this._disableInput = false;
      return;
    }

    /**
     * Swap jewels in grid data
     */
    this.grid.swap(jFCoords, jSCoords);
    this._firstJewel = this.grid.getItem(jFCoords.x, jFCoords.y);
    this._secondJewel = this.grid.getItem(jSCoords.x, jSCoords.y);

    /**
     * Function on end of swap animation
     */
    const onCompleteSwapping = () => {
      const itemsToRemove = this.grid
        .findMathesFor(jFCoords.x, jFCoords.y)
        .concat(this.grid.findMathesFor(jSCoords.x, jSCoords.y));

      /**
       * If nothing to remove swap it again
       */
      if (itemsToRemove.length === 0) {
        this.grid.swap(jFCoords, jSCoords);
        // eslint-disable-next-line
        return timeline.reverse();
      }
      this.quest.makeTurn();
      /**
       * Remove all jewels consist of array from screen
       * @private
       */
      itemsToRemove.forEach((v: any, i: any) => {
        const jewel = this.grid.getItem(v.x, v.y);
        if (!jewel) {
          return;
        }

        jewel.sprite.setParent(this.gameScreen);
        jewel.sprite.position.x += this.container.position.x;
        jewel.sprite.position.y += this.container.position.y;

        const finalizeCallback = () => {
          this.finalizeChecking(itemsToRemove);
          this.quest.collectBlocks(jewel.typeID, 1);
        };

        const cb = () => {
          this.quest.collectBlocks(jewel.typeID, 1);
        };

        jewel.spinAndRemove(i === itemsToRemove.length - 1 ? finalizeCallback : cb);
      });

      this.grid.removeItems(itemsToRemove);

      this._firstJewel = null;
      this._secondJewel = null;
    };

    /**
     * Create moving animation
     */
    const timeline = gsap.timeline({onReverseComplete: enableInput});
    timeline.add(
      gsap.to(this._firstJewel.sprite, {
        duration: 0.3,
        x: self._secondJewel.sprite.x,
        y: self._secondJewel.sprite.y,
        ease: Power2.easeIn,
      })
    );
    timeline.add(
      gsap.to(this._secondJewel.sprite, {
        duration: 0.3,
        x: self._firstJewel.sprite.x,
        y: self._firstJewel.sprite.y,
        ease: Power2.easeIn,
        onComplete: onCompleteSwapping as any,
      }),
      0
    );
  }

  checkSingleJewel = async ({x, y}: any) => {
    const itemsToRemove = this.grid.findMathesFor(x, y);

    if (itemsToRemove.length === 0) {
      return 0;
    }
    // Force pause before remove jewels going down
    if (itemsToRemove.length > 0) {
      // eslint-disable-next-line no-promise-executor-return
      await new Promise((resolve) => setTimeout(resolve, 300));
    }

    let jewelsTriggered = 0;
    const animations = itemsToRemove.map(
      // eslint-disable-next-line @typescript-eslint/no-shadow
      ({x, y}: any) =>
        new Promise((resolve) => {
          const jewel = this.grid.getItem(x, y);
          if (jewel) {
            const animationComplete = () => {
              this.quest.collectBlocks(jewel.typeID, 1);
              resolve(1);
            };

            jewelsTriggered += 1;
            // jewel.moveTo(this.container.width / 2, -100, animationComplete);
            jewel.spinAndRemove(animationComplete);
          } else {
            resolve(1);
          }
          this.grid.removeItem(x, y);
        })
    );
    await Promise.all(animations);

    return jewelsTriggered;
  };

  finalizeChecking = async (jewels: any[]) => {
    const promises = this.grid.refillColumns();

    const data = await Promise.all(promises);

    let itemsToCheck = data.flat();

    let counter = 1;
    event.trigger(Events.AddScore, {
      counter,
      jewelsCount: jewels.length,
    });

    do {
      counter += 1;
      // eslint-disable-next-line no-await-in-loop
      const count = await Promise.all(itemsToCheck.map((item) => this.checkSingleJewel(item)));
      // eslint-disable-next-line no-await-in-loop, no-promise-executor-return
      await new Promise((resolve) => setTimeout(resolve, 300));
      // eslint-disable-next-line @typescript-eslint/no-shadow
      const promises = this.grid.refillColumns();
      // eslint-disable-next-line no-await-in-loop
      const items2 = await Promise.all(promises);
      const matches = this.grid.checkAvailbleSwipes();
      this.drawMatches(matches);

      itemsToCheck = items2.filter((v) => v.length > 0).flat();
      event.trigger(Events.AddScore, {
        counter,
        jewelsCount: count.reduce((acc: any, v) => acc + v, 0),
      });
    } while (itemsToCheck.length > 0);

    if (this.quest.checkCompleted()) {
      return this.options.onLevelCompleted();
    }

    if (this.quest.checkGameover()) {
      return this.options.onGameOver();
    }

    this._disableInput = false;

    this._firstJewel = null;
    this._secondJewel = null;
  };
}

export default JewelsGame;
