import {Sprite} from 'pixi.js';
import gsap, {Power4, Power0} from 'gsap';

import {Colors} from '~/dictionary';

interface JewelParams {
  id: string;
  x: number;
  y: number;
  width: number;
  height: number;
}

interface MoveDownParams {
  offset: number;
  times?: number;
  onComplete?: () => void;
}

class Jewel {
  typeID: string;
  sprite: Sprite;
  isMouseDown: boolean;
  timeline: GSAPTimeline;

  constructor({id, x, y, width, height}: JewelParams) {
    this.typeID = id;
    this.isMouseDown = true;
    this.timeline = gsap.timeline();

    if (id !== 'x') {
      // eslint-disable-next-line new-cap
      this.sprite = Sprite.from(`images/jewels/jewel${id}.png`);
      this.sprite.anchor.x = 0.5;
      this.sprite.anchor.y = 0.5;
      this.sprite.width = width;
      this.sprite.height = height;
      this.sprite.position.x = x;
      this.sprite.position.y = y;

      this.sprite.interactive = true;
      this.sprite.on('pointerover', this.onMouseOver.bind(this));
      this.sprite.on('pointerout', this.onMouseOut.bind(this));
      this.sprite.on('pointerdown', this.onMouseDown.bind(this));
      this.sprite.on('pointerup', this.onMouseUp.bind(this));
      this.sprite.on('pointerupoutside', () => {
        this.sprite.tint = 0xffffff;
      });
    } else {
      this.sprite = new Sprite();
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onMouseOver(_e: any) {
    console.log('mouse over');
  }

  onMouseOut(e: any) {
    console.log('onMouseOut');
    if (this.isMouseDown) {
      this.onMouseUp(e);
    }
  }

  onMouseDown() {
    this.isMouseDown = true;
    this.sprite.tint = 0xbbbbbb;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onMouseUp(_e: any) {
    this.isMouseDown = false;
    this.sprite.tint = Colors.White;
  }

  moveTo(x: any, y: any, onComplete: any, time = 0.5) {
    this.sprite.parent.setChildIndex(this.sprite, this.sprite.parent.children.length - 1);
    this.timeline.add(
      gsap.to(this.sprite, {
        duration: time,
        x,
        y,
        ease: Power0.easeNone,
        onComplete,
      })
    );
  }

  flyAway(container: any, point: any, onComplete?: any) {
    const {position} = this.sprite.parent;
    this.sprite.setParent(container);

    this.sprite.position.x += position.x;
    this.sprite.position.y += position.y;
    this.timeline.to(this.sprite.position, {
      duration: 5,
      motionPath: [{x: this.sprite.position.x, y: this.sprite.position.y}, {x: 200, y: 0}, point],
      ease: Power0.easeNone,
      onComplete,
    });
  }

  spinAndRemove(onComplete?: any) {
    this.timeline.add(
      gsap.to(this.sprite, {
        duration: 0.3,
        rotation: 2,
        alpha: 0,
        ease: Power0.easeNone,
      })
    );
    this.timeline.add(
      gsap.to(this.sprite.scale, {
        duration: 0.3,
        x: 0.1,
        y: 0.1,
        ease: Power0.easeNone,
        onComplete,
      }),
      0
    );
  }

  drop = ({times = 1, onComplete, offset}: MoveDownParams) =>
    new Promise((resolve) => {
      this.sprite.position.y = (this.sprite.height + offset) * -1;

      const tween = gsap.to(this.sprite, {
        duration: 0.1 * times,
        y: `+=${(this.sprite.height + offset) * times}`,
        ease: Power4.easeInOut,
        onComplete: () => {
          if (onComplete) {
            onComplete();
          }
          resolve(1);
        },
      });
      const squashIn = gsap.to(this.sprite, {
        duration: (0.1 * times) / 3,
        height: `-=5`,
      });

      this.timeline.add(tween);
      this.timeline.add(squashIn, `-=${(0.1 * times) / 3}`);
      this.timeline.add(squashIn.reverse(), '>');
    });

  moveDown(onComplete: any) {
    const tween = gsap.to(this.sprite, {
      duration: 0.1,
      y: `+=${this.sprite.height + 5}`,
      ease: Power0.easeNone,
      onComplete,
    });
    this.timeline.add(tween);
  }

  // moveDownTo = ({times = 1, timeline, offset}: MoveDownParams) => {
  moveDownTo = ({times = 1, timeline, offset}: any) => {
    const tween = gsap.to(this.sprite, {
      duration: 0.1 * times,
      y: `+=${(this.sprite.height + offset) * times}`,
      ease: Power4.easeInOut,
    });
    const squashIn = gsap.to(this.sprite, {
      duration: (0.1 * times) / 3,
      height: `-=5`,
    });

    timeline.add(tween);
    timeline.add(squashIn, `-=${(0.1 * times) / 3}`);
    timeline.add(squashIn.reverse(), '>');
  };
}

export default Jewel;
