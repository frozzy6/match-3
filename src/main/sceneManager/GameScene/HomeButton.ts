import {Texture, Sprite} from 'pixi.js';

import Button from '~/main/accessory/Button';

const btnTexture = Texture.from('images/home_btn.png');
const btnTexturePressed = Texture.from('images/home_btn_pressed.png');

const BUTTON_HEIGHT = 60;
const BUTTON_WIDTH = 60;

class HomeButton extends Button {
  texture: any;

  constructor(x: any, y: any) {
    super(x, y);

    this.texture = new Sprite(btnTexture);
    this.texture.buttonMode = true;
    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;

    this.graphics.addChild(this.texture);
  }

  onMouseUp(e: any) {
    if (!this.isDown) return;
    this.texture.texture = btnTexture;

    super.onMouseUp(e);
  }

  onMouseDown(e: any) {
    this.texture.texture = btnTexturePressed;

    super.onMouseDown(e);
  }
}

export default HomeButton;
