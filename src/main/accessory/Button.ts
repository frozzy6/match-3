import {Container} from 'pixi.js';

class Button {
  isDown: boolean;

  isOver: boolean;

  graphics: Container;

  constructor(x: number, y: number) {
    this.isDown = false;
    this.isOver = false;

    this.graphics = new Container();
    this.graphics.interactive = true;

    this.graphics.position.x = x;
    this.graphics.position.y = y;

    this.graphics.on('pointerover', () => {
      this.onSpriteOver();
    });
    this.graphics.on('pointerout', () => {
      this.onSpriteOut();
    });
    this.graphics.on('pointerdown', () => {
      this.onMouseDown();
    });
    this.graphics.on('pointerup', () => {
      this.onMouseUp();
    });
    this.graphics.on('pointerupoutside', () => {
      this.onSpriteOut();
    });
  }

  onMouseDown(e?: any) {
    this.isDown = true;
    this.onClick(e);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onClick(e: any) {
    // throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onMouseUp(e?: any) {
    this.isDown = false;
  }

  onSpriteOver() {
    this.isOver = true;
  }

  setPosition(x: number, y: number) {
    this.graphics.x = x;
    this.graphics.y = y;
  }

  onSpriteOut() {
    this.isOver = false;
    if (this.isDown) {
      // this.graphics.emit('pointerup');
      this.isDown = false;
    }
  }
}

export default Button;
