type TListener = (args: any) => void;

type TCollectionOfListeners = {
  listeners: {
    [key: string]: TListener[];
  };
  get: (name: string) => TListener[];
  remove: (name: string) => void;
};

// Collection of all listeners
const CollectionOfListeners: TCollectionOfListeners = {
  /* Collection of EventName: [ Listeners ] */
  listeners: {},
  get(name) {
    // If collection not existed - create, than return collection
    if (!this.listeners[name]) {
      this.listeners[name] = [] as TListener[];
    }

    return this.listeners[name];
  },
  remove(name) {
    if (this.listeners[name]) {
      this.listeners[name] = [];
    }
  },
};

const removeAll = () => {
  const events = Object.keys(CollectionOfListeners);
  for (let i = events.length; i--; ) {
    CollectionOfListeners.listeners[events[i]].length = 0;
  }
};

const removeCustomEventListener = (event: any, func: any) => {
  if (func) {
    const coll = CollectionOfListeners.get(event);
    coll.splice(coll.indexOf(func), 1);
  } else {
    CollectionOfListeners.remove(event);
  }
};

const addCustomEventListener = (event: any, func: any) => {
  const coll = CollectionOfListeners.get(event);
  if (func.call) {
    coll.push(func);
  }
};

const dispatchCustomEvent = (event: any, options?: any) => {
  const coll = CollectionOfListeners.get(event);
  for (let i = coll.length; i--; ) {
    coll[i](options);
  }
};

export enum Events {
  LoadingComplete = 'LOADING_COMPLETE',
  StartNewGame = 'START_NEW_GAME',
  Continue = 'CONTINUE',
  BackToMenu = 'BACK_TO_MENU',
  AddScore = 'ADD_SCORE',
  LevelsSceneShow = 'LEVELS_SCENE_SHOW',
  GameOver = 'GAME_OVER',
}

export default {
  on: addCustomEventListener,
  off: removeCustomEventListener,
  trigger: dispatchCustomEvent,
  removeAll,
};
