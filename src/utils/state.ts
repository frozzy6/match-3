class AppState {
  data: any;

  constructor() {
    console.log('Initialize global app store');
    this.data = {};
  }

  setValue(key: string, value: any) {
    this.data[key] = value;
  }

  getValue(key: string) {
    return this.data[key];
  }
}

export default new AppState();
