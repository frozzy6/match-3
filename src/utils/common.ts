/**
 * Helper function to get random number from 1 to max
 */
export function rand(max: number) {
  return Math.floor(Math.random() * max) + 1;
}

export function between(min: number, max: number) {
  return Math.random() * (max - min) + min;
}
