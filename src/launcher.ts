import Application from './Application';
import {TOptions, Color} from './types';
import './styles.css';

const Options: TOptions = {
  Grid: {
    width: 10,
    height: 8,
  },
  ItemsPool: [{color: Color.Red}, {color: Color.Green}, {color: Color.Blue}],
};

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new Application(Options);
});
