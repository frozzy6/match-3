export enum Colors {
  Black = 0x000000,
  White = 0xffffff,
  LightGrey = 0xcccccc,
  LightBlue = 0x1099bb,
  Purple = 0xae5d8b,
  Red = 0xff0000,
  Green = 0x00ff00,
  Blue = 0x0000ff,
  Jagger = 0x3f3643,
  Lime = 0xa0b538,
}
