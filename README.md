# simply match 3 game

Match 3 game written in JavaScript.

Builded with:
* [PixiJS](https://www.pixijs.com/)
* [GSAP](https://greensock.com/gsap/)

[Try it online](https://frozzy6.gitlab.io/match-3/)

![Menu](/screenshots/menu.png)
![Game](/screenshots/game.png)

