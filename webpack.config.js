// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

// eslint-disable-next-line @typescript-eslint/no-var-requires
const HtmlWebpackPlugin = require('html-webpack-plugin');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/launcher.ts',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        use: ['source-map-loader'],
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          cacheCompression: false,
          configFile: path.resolve(__dirname, 'babel.config.js'),
        },
      },
      {
        test: /\.([cm]?ts|tsx)$/,
        loader: 'ts-loader',
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ],
  },
  stats: 'errors-only',
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.jsx', '.css', '.json'],
    alias: {
      '~/dictionary': path.resolve(__dirname, 'src/dictionary'),
      '~/main': path.resolve(__dirname, 'src/main'),
      '~/utils': path.resolve(__dirname, 'src/utils/'),
      '~/types': path.resolve(__dirname, 'src/types/'),
    },
  },
  plugins: [
    new ESLintPlugin({
      files: './src/**/*.{ts,tsx,js,jsx}',
      overrideConfigFile: path.resolve(__dirname, './.eslintrc.js'),
      // Show warnings on the console
      emitWarning: true,
      emitError: true,
      // But do not crash the process
      failOnError: false,
      cache: true,
      cacheLocation: path.resolve(__dirname, '../node_modules/.cache/.eslintcache'),
    }),
    new HtmlWebpackPlugin({
      template: './launcher.html',
    }),
  ],
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    static: [{directory: path.join(__dirname, 'assets')}],
    hot: true,
    client: {overlay: {warnings: false}},
  },
};
